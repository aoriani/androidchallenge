package com.foo.umbrella.ui;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.foo.umbrella.R;
import com.foo.umbrella.UmbrellaApp;
import com.foo.umbrella.data.api.WeatherService;
import com.foo.umbrella.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements LifecycleRegistryOwner {

    private WeatherViewModel mWeatherViewModel;
    private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);
    private ActivityMainBinding mBiding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBiding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(mBiding.toolbar);

        //TODO Inject
        final WeatherService weatherService = ((UmbrellaApp) getApplicationContext()).getApiServicesProvider().getWeatherService();
        final WeatherViewModelFactory viewModelFactory = new WeatherViewModelFactory(weatherService);

        mWeatherViewModel = ViewModelProviders.of(this, viewModelFactory).get(WeatherViewModel.class);
        mWeatherViewModel.getCurrentObservation().observe(this, data -> {
            if (data != null ) mBiding.setCurrentObservation(data);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mWeatherViewModel.fetch("94066");
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return mRegistry;
    }
}

package com.foo.umbrella.ui;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Log;

import com.foo.umbrella.data.api.WeatherService;
import com.foo.umbrella.data.model.CurrentObservation;
import com.foo.umbrella.data.model.WeatherData;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class WeatherViewModel extends ViewModel {

    private final WeatherService mWeatherService;
    private final MutableLiveData<WeatherData> mWeatherData = new MutableLiveData<>();
    private final LiveData<CurrentObservation> mCurrentObservation = Transformations.map(mWeatherData,
            WeatherData::getCurrentObservation);

    public WeatherViewModel(WeatherService service) {
        mWeatherService = service;
    }


    public void fetch(@NonNull String zipcode) {
        mWeatherService
                .forecastForZipObservable(zipcode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                        if (data.isError()) {
                            Log.d("ORIANI", "haha " + data.error().getMessage());
                        } else {
                            mWeatherData.setValue(data.response().body());
                        }
                    },
                        error -> Log.d("ORIANI", "jkdjdfk " + error.getMessage())
                );
    }

    public LiveData<CurrentObservation> getCurrentObservation() {
        return mCurrentObservation;
    }

}

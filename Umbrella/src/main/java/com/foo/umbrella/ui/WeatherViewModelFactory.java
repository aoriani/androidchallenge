package com.foo.umbrella.ui;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.foo.umbrella.data.api.WeatherService;

//https://github.com/googlesamples/android-architecture-components/blob/master/BasicRxJavaSample/app/src/main/java/com/example/android/observability/ui/ViewModelFactory.java
public class WeatherViewModelFactory implements ViewModelProvider.Factory {

    private final WeatherService mWeatherService;

    public WeatherViewModelFactory(WeatherService service) {
        mWeatherService = service;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(modelClass)) {
            return (T) new WeatherViewModel(mWeatherService);
        }
        throw new IllegalArgumentException("UnknowViewModelClass");
    }
}
